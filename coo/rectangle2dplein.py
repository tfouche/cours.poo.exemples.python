"""Définition de la classe Rectangle2DPlein"""

from coo.rectangle2d import Rectangle2D


class Rectangle2DPlein(Rectangle2D):
    def __init__(self, orig, tailleX, tailleY, couleur):
        """Initialise un rectangle à partir d'un point, d'une taille et d'une couleur"""
        Rectangle2D.__init__(self, orig, tailleX, tailleY)
        self.couleur = couleur

    def __str__(self):
        return "Rectangle2DPlein({}, {}, {})".format(self.orig, self.fin, self.couleur)

    def colorie(self, couleur):
        """Modifie la couleur du rectangle"""
        self.couleur = couleur
