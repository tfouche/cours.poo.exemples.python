from unittest import TestCase, main
from coo import cercle2d, point2d

class CercleTestCase(TestCase):
    def setUp(self):
        centre = point2d.Point2D(1.0, 2.0)
        self.c = cercle2d.Cercle2D(centre, 3.0)

    def assert_cercle(self, expectedX, expectedY, expectedRadius):
        self.assertEqual(self.c.centre.x,expectedX)
        self.assertEqual(self.c.centre.y,expectedY)
        self.assertEqual(self.c.rayon, expectedRadius)

    def test_constructor(self):
        self.assert_cercle(1.0, 2.0, 3.0)

    def test_str(self):
        self.assertEqual(str(self.c), "Cercle2D(Point2D(1.0, 2.0), 3.0)")

    def test_translate(self):
        self.c.translate(2.0, 3.0)
        self.assert_cercle(3.0, 5.0, 3.0)

if __name__ == '__main__':
    main()
